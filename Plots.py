import theano
import logging
import pickle
import theano.tensor as T
import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams
import matplotlib.pyplot as plt
from time import time
from math import pi, log
from scipy.optimize import minimize
from scipy.stats import linregress
import Constants as C
import SimpleStarSimulation as SSS
from Errors import *


def HRdiagram(T, L, M, prefix = '', show=False):

    plt.scatter(np.log10(T / C.TSun), np.log10(L / C.LSun))
    plt.title('Main Sequence in a Hertzsprung-Russell diagram')
    plt.xlabel('Temperature $log(T/T_{\odot})$')
    plt.ylabel('Luminosity $log(L/L_{\odot})$')
    plt.annotate('$%.2f M_{\\odot}$' % (M[0]/C.mSun), xy = (np.log10(T[0] / C.TSun), np.log10(L[0] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[-1]/C.mSun), xy = (np.log10(T[-1] / C.TSun), np.log10(L[-1] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[int(len(M)/3)]/C.mSun), xy = (np.log10(T[int(len(M)/3)] / C.TSun), np.log10(L[int(len(M)/3)] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[int(2*len(M)/3)]/C.mSun), xy = (np.log10(T[int(2*len(M)/3)] / C.TSun), np.log10(L[int(2*len(M)/3)] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[31]/C.mSun), xy = (np.log10(T[31] / C.TSun), np.log10(L[31] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.grid()
    plt.gca().invert_xaxis()
    plt.savefig(prefix + 'Main_Sequence_norm.png')
    if show:
        plt.show()
    plt.clf()

    plt.scatter(np.log10(T), np.log10(L / C.LSun))
    plt.title('Main Sequence in a Hertzsprung-Russell diagram')
    plt.xlabel('Temperature $log(T)$')
    plt.ylabel('Luminosity $log(L/L_{\odot})$')
    plt.annotate('$%.2f M_{\\odot}$' % (M[0]/C.mSun), xy = (np.log10(T[0]), np.log10(L[0] / C.LSun)), xytext=(0,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[-1]/C.mSun), xy = (np.log10(T[-1]), np.log10(L[-1] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[int(len(M)/3)]/C.mSun), xy = (np.log10(T[int(len(M)/3)]), np.log10(L[int(len(M)/3)] / C.LSun)), xytext=(20,20), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[int(2*len(M)/3)]/C.mSun), xy = (np.log10(T[int(2*len(M)/3)]), np.log10(L[int(2*len(M)/3)] / C.LSun)), xytext=(-80,-5), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.annotate('$%.2f M_{\\odot}$' % (M[31]/C.mSun), xy = (np.log10(T[31]), np.log10(L[31] / C.LSun)), xytext=(-100,5), textcoords = 'offset points', arrowprops={'arrowstyle':'->','connectionstyle':'arc3,rad=0'})
    plt.grid()
    plt.gca().invert_xaxis()
    plt.savefig(prefix + 'Main_Sequence.png')
    if show:
        plt.show()
    plt.clf()
    
def MassLuminosity(M, L, prefix = '', show=False):

    plt.scatter(np.log10(M / C.mSun), np.log10(L / C.LSun))
    plt.title('Main Sequence mass luminosity relation')
    plt.xlabel('Mass $log(M/M_{\odot})$')
    plt.ylabel('Luminosity $log(L/L_{\odot})$')
    plt.grid()
    plt.savefig(prefix + 'MassLuminosity_Sequence.png')
    if show:
        plt.show()
    plt.clf()

def MassRadius(M, R, prefix = '', show=False):

    plt.scatter(np.log10(M / C.mSun), np.log10(R / C.RSun))
    plt.title('Main Sequence mass radius relation')
    plt.xlabel('Mass $log(M/M_{\odot})$')
    plt.ylabel('Radius $log(R/R_{\odot})$')
    plt.grid()
    plt.savefig(prefix + 'MassRadius_Sequence.png')
    if show:
        plt.show()
    plt.clf()

def MassTemperature(M, T, prefix = '', show=False):

    plt.scatter(np.log10(M / C.mSun), np.log10(T / C.TSun))
    plt.title('Main Sequence mass temperature relation')
    plt.xlabel('Mass $log(M/M_{\odot})$')
    plt.ylabel('Temperature $log(T/T_{\odot})$')
    plt.grid()
    plt.savefig(prefix + 'MassTemperature_Sequence.png')
    if show:
        plt.show()
    plt.clf()
    
def CentralParameters(T, Rho, prefix = '', show=False):

    plt.scatter(T, Rho)
    plt.title('Central Parameters')
    plt.xlabel('Central Temperature ($K$)')
    plt.ylabel('Central Density ($kg / m^3$)')
    plt.savefig(prefix + 'CentralParameters.png')
    if show:
        plt.show()
    plt.clf()

def StepSize(R, prefix = '', show=False):

    dr = np.zeros(len(R) - 1)
    for i in range(len(R)-1):
        dr[i] = R[i+1] - R[i]
    plt.scatter(R[:-1] / R[-1], dr)
    plt.title('Step Size Changes In Stellar Interior')
    plt.xlabel('Radius ($R^* = %.1e m$)' % R[-1])
    plt.ylabel('Step Size (m)')
    plt.grid()
    plt.savefig(prefix + 'StepSize.png')
    if show:
        plt.show()
    plt.clf()
    
def StellarInterior(T, Rho, M, L, epsilon, R, prefix = '', show=False):

    dLdr = (L[1:] - L[:-1]) / (R[1:] - R[:-1])
    peak_dL = np.max(dLdr)
    
    plt.plot(R / R[-1], T / T[0], c='r', label='$T / %.1e$' % T[0])
    plt.plot(R / R[-1], Rho / Rho[0], c='b', label='$\\rho / %.1e$' % Rho[0])
    plt.plot(R / R[-1], M / M[-1], c='g', label='$M / %.1e$' % M[-1])
    plt.plot(R / R[-1], L / L[-1], c='k', label='$L / %.1e$' % L[-1])
    plt.plot(R[:-1] / R[-2], dLdr / peak_dL, c='k', ls='--', label='$dL/dr / %.1e$' % peak_dL)
    plt.plot(R / R[-1], epsilon / epsilon[0], c='m', label='$\\epsilon / %.1e$' % epsilon[0])
    plt.legend(loc = 'center right')
    plt.grid()
    plt.title('Stellar Interior')
    plt.xlabel('Radius ($R / %.1e$)' % R[-1])
    plt.ylabel('Normalized Profile')
    plt.savefig(prefix + 'Stellar_Interior.png')
    if show:
        plt.show()
    plt.clf()


def Opacity(kappa, R, prefix = '', show=False):

    plt.plot(R, np.log10(kappa))
    plt.grid()
    plt.title('Stellar Interior Opacity')
    plt.xlabel('Radius ($m$)')
    plt.ylabel('Opacity ($m^2/kg$)')
    plt.savefig(prefix + 'Opacity.png')
    if show:
        plt.show()
    plt.clf()
    
def TransferMeasure(dlnPdlnT, R, prefix = '', show=False):

    plt.plot(R, dlnPdlnT, c='b')
    plt.grid()
    plt.title('Convective/Radiative Transfer Measure')
    plt.xlabel('Radius ($R^* = %e m$)' % R[-1])
    plt.ylabel('Transfer Measure ($\\frac{d\\ln P}{d\\ln T}$)')
    plt.savefig(prefix + 'TransferMeasure.png')
    if show:
        plt.show()
    plt.clf()
    
    
    
