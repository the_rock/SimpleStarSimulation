To run the simulation simply enter:

python MainSequence.py

in your ternimal and the main sequence will be generated along with a whole bunch of plots. Alternatively you can start your favorite python interpreter, import the MainSequence file then run the function 'MainSequence' to generate the main sequence and 'AnalyzeSequence' to produce the cool plots. Sorry, I didn't have as much time as I thought to comment it. However, the code is quite simple to follow.

Note to self: Look into the MESA code