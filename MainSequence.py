import theano
import logging
import pickle
import theano.tensor as T
import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams
import matplotlib.pyplot as plt
from time import time
from math import pi, log
from scipy.optimize import minimize
from scipy.stats import linregress
import Constants as C
import Plots as P
import SimpleStarSimulation as SSS
from Errors import *

def AnalyzeSequence(loadfrom = 'mainsequence.txt', prefix=''):

    with open(loadfrom, 'r') as f:
        lines = f.readlines()
        Tc = np.array(eval(lines[0]))
        rhoc = np.array(eval(lines[1]))
        X = eval(lines[2])
        Z = eval(lines[3])

    surface_params = []
    for i in range(len(Tc)):
        S = SSS.Star(rho0 = rhoc[i], T0=Tc[i], X=X, Z=Z, tolerance=1e-6)
        S.ShootStar()
        surface_params.append(S.SurfaceParams())
        S.MakePlots(prefix + '%.3i_' % i)

    T = np.array(list(s['T'] for s in surface_params))
    Tprime = np.array(list(s['Tprime'] for s in surface_params))
    L = np.array(list(s['L'] for s in surface_params))
    M = np.array(list(s['M'] for s in surface_params))
    R = np.array(list(s['R'] for s in surface_params))

    P.HRdiagram(Tprime, L, M, prefix)
    P.MassLuminosity(M, L, prefix)
    P.MassRadius(M, R, prefix)
    P.MassTemperature(M, Tprime, prefix)
    P.CentralParameters(Tc, rhoc, prefix)

def MainSequence(Tc = np.array(map(lambda x: 10**x, np.linspace(6.7,7.5,67))), X = 0.71, Z = 0.02, saveto = 'mainsequence.txt'):

    rhoc = np.zeros(len(Tc))
    
    for i in range(len(Tc)):
        print i
        rhoc[i] = Bisection([3.0e2, 5.0e5], Tc[i], X, Z, index = i)
        print rhoc[i]
        
    print 'Completed Main Sequence!'
    print 'rhoc: %s' % str(rhoc)

    with open(saveto, 'w') as f:
        f.write(str(list(t for t in Tc)) + '\n')
        f.write(str(list(r for r in rhoc)) + '\n')
        f.write(str(X)+'\n')
        f.write(str(Z))
        
    print rhoc

def Bisection(rho_range, T, X = 0.73, Z = 0.02, tol = 0.1, alpha = 0.8, index = -1):

    rho_vals = []
    f_vals = []
    converge = []

    mid_range = (rho_range[0] + rho_range[1]) / 2.0
    l = f(rho_range[0], T, X, Z)
    m = f(mid_range, T, X, Z)
    r = f(rho_range[1], T, X, Z)

    while abs(rho_range[1] - rho_range[0]) > tol:
        if l[0] * r[0] > 0:
            print 'grrr'
        print rho_range
        if l[0] * m[0] < 0:
            rho_range[1] = mid_range
            r = m
        elif m[0] * r[0] < 0:
            rho_range[0] = mid_range
            l = m
        elif r[1]:
            rho_range[1] = alpha * rho_range[1] + (1.0 - alpha) * rho_range[0]
            r = f(rho_range[1], T, X, Z)
            rho_vals.append(rho_range[1])
            f_vals.append(r[0])
            converge.append(r[1])
            continue
        else:#if l[1]
            rho_range[0] = alpha * rho_range[0] + (1.0 - alpha) * rho_range[1]
            l = f(rho_range[0], T, X, Z)
            rho_vals.append(rho_range[0])
            f_vals.append(l[0])
            converge.append(l[1])
            continue
        mid_range = (rho_range[0] + rho_range[1]) / 2.0
        m = f(mid_range, T, X, Z)
        rho_vals.append(mid_range)
        f_vals.append(m[0])
        converge.append(m[1])
        

    plt.scatter(rho_vals, f_vals)
    plt.yscale('symlog',linthresh=0.01)
    plt.grid()
    plt.title('Convergence Plot')
    plt.xlabel('Central Density ($kg / m^3$)')
    plt.ylabel('Convergence Criterion ($\\frac{L_* - 4\\pi\\sigma R_*^2T_*^4}{\\sqrt{L_*4\\pi\\sigma R_*^2T_*^4}}$)')
    plt.savefig('%0.3i_ConvergencePlot.png' % index)
    plt.clf()
    

    return_rho = -1
    return_f = 1e20
    for i in range(len(rho_vals)):
        if not converge[i] and abs(f_vals[i]) < abs(return_f):
            return_rho = rho_vals[i]
            return_f = f_vals[i]
            
    return return_rho


def f(rhoc, Tc, X, Z):

    err = False
    print 'now computing star with: rho %e, T %e, X %e, Z %e' % (rhoc, Tc, X, Z)
    S = SSS.Star(rho0 = rhoc, T0=Tc, X=X, Z=Z, tolerance=1e-6)
    try:
        S.ShootStar()
    except ConvergeError as e:
        err = True
        print 'Got error %s, continuing...' % e

    return S.Evaluate(), err

if __name__ == '__main__':
    MainSequence()
    AnalyzeSequence()
