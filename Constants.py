import theano
import theano.tensor as T
from math import pi, log
import numpy as np

# Gravitational constant
G = 6.67408e-11 # m^3 kg^-1 s^-2

# Speed of light
c = 2.99792458e8 # m s^-1

# Stephan-Boltzmann constant
sigma = 5.670367e-8 # W m^-2 K^-4

# Radiation constant
a = 4 * sigma / c #7.565767e-16 # J m^-3 K^-4

# Boltzmann constant
k = 1.38064852e-23 # m^2 kg s^-2 K^-1

# Proton mass
mp = 1.67262178e-27 # kg

# Electron mass
me = 9.10938291e-31 # kg

# Hydrogen mass
mH = 1.00782503207 * 1.660539040e-27 # kg

# Solar mass
mSun = 1.989e30 # kg

# Solar radius
RSun = 604563300.0 # m from text 0.869R_sun current

# Solar luminosity
LSun = 2.591556e26 # from text 0.677*L_sun current

# Solar Temperature
TSun = 5620 # K from text 

# Specific heat ratio
gamma = 5.0 / 3.0
gamma_cond = gamma / (gamma - 1.0)

# Plancks constant
h = 6.626070040e-34 # J s
hbar = 1.05457173e-34      # h/2pi


def GetKappaTable(X, Y, Z):

    with open('OPALtable.txt','r') as f:
        kappa_logR, kappa_logT, opal_ref, opal_table = map(eval, f.readlines())
        
    best_approx = 1e100
    best_index = -1
    
    for i in range(1,127):
        approx = (opal_ref[i]['X'] - X)**2 + (opal_ref[i]['Y'] - Y)**2 + (opal_ref[i]['Y'] - Y)**2
        if approx < best_approx:
            best_approx = approx
            best_index = i

    if best_index != -1:
        kappa = opal_table[best_index]
    else:
        raise ValueError('Could not find suitable location in the kappa tables for your composition: X %f, Y %f, Z %f' % (X, Y, Z))

    return np.array(kappa), np.array(kappa_logR), np.array(kappa_logT)

def GetKappa(y, kappa_table, kappa_logR, kappa_logT):

    # print 'argmin T: %i' % np.argmin(np.abs(kappa_logT - log(T,10)))
    # print 'argmin R: %i' % np.argmin(np.abs(kappa_logR - log(rho * 1e-3 / ((T * 1e-6)**3), 10)))

    return kappa_table[np.argmin(np.abs(kappa_logT - log(y[0],10)))][np.argmin(np.abs(kappa_logR - log(y[1] * 1e-3 / ((y[0] * 1e-6)**3), 10)))] / 10.0
