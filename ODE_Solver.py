import theano
import logging
import pickle
import theano.tensor as T
import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams
from time import time
import Constants as C

def Adaptive_Runge_Kutta(dydr, y, r, dr, tol, const_args = tuple()):

    err_norm = np.inf
    count = 0
    cond = True
    old_dr = 0
    new_dr = dr

    
    while err_norm > tol:
        count += 1

        y_rk4, y_rk5 = Runge_Kutta_4_5(dydr, y, r, new_dr, const_args)
    
        err_norm = np.linalg.norm((y_rk5 / y_rk4) - 1.0)
        
        old_dr = new_dr
        
        new_dr = 0.9 * old_dr * min(max(tol / err_norm, 0.3), 2.0)

        new_dr = min(new_dr,5e6) #max(min(new_dr,5e6), 100.0)
        
        if count > 50:
            cond = False
            break

    
    return y_rk5, old_dr, new_dr, cond
    

def Runge_Kutta_4_5(dydr, y, r, dr, const_args = tuple()):

    # 4th order
    y_rk4 = Runge_Kutta(dydr, y, r, dr, const_args)
    # 5th order
    y_prime = Runge_Kutta(dydr, y, r, dr / 2.0, const_args)
    y_rk5 = Runge_Kutta(dydr, y_prime, r + dr / 2.0, dr / 2.0, const_args)
    
    return y_rk4, y_rk5

def Runge_Kutta(dydr, y, r, dr, const_args = tuple()):
    
    dr2 = dr / 2.0

    k1 = np.array(list(dydr[i](y, r, dydr, *const_args) for i in range(len(dydr))))
    k2 = np.array(list(dydr[i](y + dr2 * k1, r + dr2, dydr, *const_args) for i in range(len(dydr))))
    k3 = np.array(list(dydr[i](y + dr2 * k2, r + dr2, dydr, *const_args) for i in range(len(dydr))))
    k4 = np.array(list(dydr[i](y + dr * k3, r + dr, dydr, *const_args) for i in range(len(dydr))))

    return  y + dr * (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0
    
                      
# def Runge_Kutta(dfdr, r, f, dr, x1 = None, x2 = None, x3 = None, x4 = None, x5 = None, x6 = None, x7 = None, x8 = None):

#     dr2 = dr / 2.0
    
#     k1 = dfdr(r      , f           , x1, x2, x3, x4, x5, x6, x7, x8)

#     k2 = dfdr(r + dr2, f + dr2 * k1, x1, x2, x3, x4, x5, x6, x7, x8)

#     k3 = dfdr(r + dr2, f + dr2 * k2, x1, x2, x3, x4, x5, x6, x7, x8)

#     k4 = dfdr(r + dr , f + dr * k3 , x1, x2, x3, x4, x5, x6, x7, x8)

#     return dr * (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0, k1
