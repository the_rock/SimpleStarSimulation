import theano
import logging
import pickle
import theano.tensor as T
import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams
from time import time
from math import pi, log
import Constants as C

# ODEs
################################################################################
# T, rho, Mr, P, tau, Lr
# 0, 1,   2,  3, 4,   5

def dPdr(y, r, dydr, P, _kappa, mu, epsilon):

    return - C.G * y[2] * y[1] / (r**2)

def diPdiT(y, r, dydr, P, _kappa, mu, epsilon):

    radiation_pressure = 4.0 * C.a * (y[0]**3) / 3.0
    gas_pressure = C.k * y[1] / (mu * C.mp)
    
    return gas_pressure + radiation_pressure

def diPdirho(y, r, dydr, P, _kappa, mu, epsilon):

    gas_pressure = C.k * y[0] / (mu * C.mp)
    degeneracy_pressure = ((3.0 * (pi**2) * y[1])**(2.0 / 3.0)) * (C.hbar**2) * ((1.0 / C.mp)**(5.0 / 3.0)) / (3.0 * C.me)

    return gas_pressure + degeneracy_pressure

def drhodr(y, r, dydr, P, _kappa, mu, epsilon):

    grav = C.G * y[2] * y[1] / (r**2)
    grad = diPdiT(y, r, dydr, P, _kappa, mu, epsilon) * dydr[0](y, r, dydr, P, _kappa, mu, epsilon)
    divi = diPdirho(y, r, dydr, P, _kappa, mu, epsilon)

    return - (grav + grad) / divi

def dMrdr(y, r, dydr, P, _kappa, mu, epsilon):

    return 4.0 * pi * (r**2) * y[1]

def dLrdr(y, r, dydr, P, _kappa, mu, _epsilon):

    return 4.0 * pi * (r**2) * y[1] * epsilon(y, 0.73, 0.03*0.73)

def dTdr(y, r, dydr, P, _kappa, mu, epsilon):

    rad = dTdr_radiative(y, r, dydr, P, _kappa, mu, epsilon)
    adi = dTdr_adiabatic(y, r, dydr, P, _kappa, mu, epsilon)

    return max(rad, adi)

def dTdr_radiative(y, r, dydr, P, _kappa, mu, epsilon):

    return - 3.0 * kappa(y, 0.73, 0.02) * y[1] * y[3] / (16.0 * pi * 4.0 * C.sigma * (y[0]**3) * (r**2))#C.a * C.c

def dTdr_adiabatic(y, r, dydr, P, _kappa, mu, epsilon):

    return - (2.0 / 5.0) * y[0] * C.G * y[2] * y[1] / (P * (r**2))#(1.0/C.gamma - 1.0) * mu * C.mH * C.G * y[2] / (C.k * (r**2))

def dtaudr(y, r, dydr, P, _kappa, mu, epsilon):

    return kappa(y, 0.73, 0.02) * y[1]

def dlnPdlnT(y, r, dydr, P, _kappa, mu, epsilon):# fixme, not sure about this

    drhodT = drhodr(y, r, dydr, P, _kappa, mu, epsilon) / dTdr(y, r, dydr, P, _kappa, mu, epsilon)

    return (y[0] / P) * (diPdiT(y, r, dydr, P, _kappa, mu, epsilon) + diPdirho(y, r, dydr, P, _kappa, mu, epsilon) * drhodT)

# Exact Form
################################################################################

def P(y, mu):

    gas_pressure = C.k * y[1] * y[0] / (mu * C.mp)
    radiation_pressure = C.a * (y[0]**4) / 3.0
    degeneracy_pressure = ((3.0 * (pi**2.0))**(2.0 / 3.0)) * (C.hbar**2) * ((y[1] / C.mp)**(5.0 / 3.0)) / (5.0 * C.me)

    return gas_pressure + radiation_pressure + degeneracy_pressure

def mu(X, Y, Z):

    return 1.0 / (2.0 * X + 0.75 * Y + 0.5 * Z)

def Y(X, Z):

    return 1.0 - X - Z

def L(r, T):

    return 4.0 * pi * C.sigma * (r**2) * (T**4)

def M(y, r):

    return 4.0 * pi * (r**3) * y[1] / 3.0

def epsilon(y, X, Xcno):

    PP = 1.07e-7 * (y[1]/1e5) * (X**2) * ((y[0]/1e6)**4)
    CNO = 8.24e-26 * (y[1]/1e5) * X * Xcno * ((y[0]/1e6)**19.9)
    
    return PP + CNO

def kappa(y, X, Z):

    kes = 0.02 * (X + 1.0)
    kff = 1.0e24 * (X + 1.0) * (Z + 1.0e-4) * ((y[1]/1e3)**0.7) * (y[0]**(-3.5))
    kH = 2.5e-32 * (Z / 0.02) * ((y[1]/1e3)**0.5) * (y[0]**9)

    if y[0] > 1e4:
        return 1.0 / ((1.0 / kH) + (1.0 / max(kes, kff)))
    else:
        return 1.0 / ((1.0 / kH) + (1.0 / min(kes, kff)))

def deltau(y, r, dydr, P, kappa, mu, epsilon):

    return kappa * (y[1]**2) / abs(drhodr(y, r, dydr, P, kappa, mu, epsilon))
        

# Conditions
################################################################################

def StellarSurface(tau, tolerance = 1e-4):

    R = np.argmin(np.abs(tau[-1] - tau - 2.0 / 3.0))
    
    return R
