import theano
import logging
import pickle
import ODE_Solver
import theano.tensor as T
import numpy as np
import StellarStructureEquations as SSE
from theano.tensor.shared_randomstreams import RandomStreams
from time import time
from math import pi, log
import Constants as C
import Plots as P
from Errors import *

class Star(object):

    def __init__(self, rho0 = 5.856e4, T0 = 8.23e6, X = 0.73, Z = 0.03, initial_dr = 1e1, max_itter=1e8, tolerance = 1e-10, verbose = False):

        self.X = X
        self.Z = Z
        self.Y = SSE.Y(self.X, self.Z)
        self.Xcno = 0.03 * self.X 
        self.mu = SSE.mu(self.X, self.Y, self.Z)
        self.dydr = [SSE.dTdr, SSE.drhodr, SSE.dMrdr, SSE.dLrdr, SSE.dtaudr]
        self.max_itter = int(max_itter)
        self.Mr_Max = 1e2 * C.mSun # Maximum allowed mass of star
        self.R_Max = 40 * C.RSun # Maximum allowed radius of star
        self.verbose = verbose
        self.tolerance = tolerance
        self.dr = initial_dr
        self.i = 0
        self.i_rstar = -1
        
        self.r = np.zeros(self.max_itter, dtype='f8')
        self.r[0] = 10
        # T, rho, Mr, Lr, tau
        # 0, 1,   2,  3,  4
        self.y = np.zeros((self.max_itter, 5), dtype='f8') 
        self.y[0][0] = T0
        self.y[0][1] = rho0
        self.epsilon = np.zeros(self.max_itter, dtype='f8')
        self.epsilon[0] = SSE.epsilon(self.y[0], self.X, self.Xcno)
        self.y[0][2] = SSE.M(self.y[0], self.r[0])
        self.y[0][3] = 4.0 * pi * (self.r[0]**3) * self.y[0][1] * self.epsilon[0] / 3.0
        self.y[0][4] = 0
        self.P = np.zeros(self.max_itter, dtype='f8')
        self.P[0] = SSE.P(self.y[0], self.mu)
        self.kappa = np.zeros(self.max_itter, dtype='f8')
        self.kappa[0] = SSE.kappa(self.y[0], self.X, self.Z)        
        self.dlnPdlnT = np.zeros(self.max_itter, dtype='f8')
        self.dlnPdlnT[0] = SSE.dlnPdlnT(self.y[0], self.r[0], self.dydr, self.P[0], self.kappa[0], self.mu, self.epsilon[0])
        self.deltau = np.zeros(self.max_itter, dtype='f8')
        #self.deltau[0] = SSE.deltau(self.y[0], self.r[0], self.dydr, self.P[0], self.kappa[0], self.mu, self.epsilon[0])
        
    def Step(self, i):

        self.y[i], old_dr, new_dr, cond = ODE_Solver.Adaptive_Runge_Kutta(self.dydr, self.y[i-1], self.r[i-1], self.dr, self.tolerance, (self.P[i-1], self.kappa[i-1], self.mu, self.epsilon[i-1]))
        self.dr = new_dr
        if not cond:
            raise ConvergeError('Adaptive step cannot find a successful step size')

        self.P[i] = SSE.P(self.y[i], self.mu)
        self.r[i] = self.r[i-1] + old_dr
        self.epsilon[i] = SSE.epsilon(self.y[i], self.X, self.Xcno)
        self.kappa[i] = SSE.kappa(self.y[i], self.X, self.Z)
        self.dlnPdlnT[i] = SSE.dlnPdlnT(self.y[i], self.r[i], self.dydr, self.P[i], self.kappa[i], self.mu, self.epsilon[i])
        self.deltau[i] = SSE.deltau(self.y[i], self.r[i], self.dydr, self.P[i], self.kappa[i], self.mu, self.epsilon[i])
        
        if self.verbose:
            print 'y: ' + str(self.y[i])
            print 'r: ' + str(self.r[i])
            print 'epsilon: ' + str(self.epsilon[i])
            print 'kappa: %f' % self.kappa[i]


    def ShootStar(self):

        while True:
            self.i += 1
            
            if self.verbose and self.i % 1000 == 0:
                print 'index: %i, radius: %f' % (self.i,self.r[self.i-1])
                
            self.Step(self.i)

            if self.y[self.i][2] > self.Mr_Max:
                raise ConvergeError('Stellar mass exceded limit. Got to %e with limit of %e' % (self.y[self.i][2], self.Mr_Max))
            if self.r[self.i] > self.R_Max:
                raise ConvergeError('Stellar radius exceded limit. Got to %e with limit of %e' % (self.r[self.i], self.R_Max))
            elif self.i >= self.max_itter - 1:
                raise ConvergeError('Itteration limit exceded. Got to %i with limit of %i' % (self.i, self.max_itter))
            elif np.any(self.y[self.i] < 0):
                print self.P[self.i]
                raise ConvergeError('Values are negative: %s, at radius: %e' % (str(self.y[self.i]),self.r[self.i]))
            elif np.any(np.isnan(self.y[self.i])):
                raise ConvergeError('Values are nan: %s' % str(self.y[self.i]))

            if self.deltau[self.i] < 1e-3:
                break
            


    def FindRstar(self, verbose=True):

        R_index = np.argmin(np.abs(self.y[self.i - 1][4] - (2.0 / 3.0) - self.y[:self.i - 2,4]))

        if self.i_rstar < 0:
            self.i_rstar = R_index
        #R_index = np.argmax(self.kappa[:self.i])

        if verbose:
            print 'tau - 2/3 = %e, tau_R* %e, tau_max %e' %  (self.y[self.i - 1][4] - (2.0 / 3.0) - self.y[R_index,4], self.y[R_index,4], self.y[self.i - 1][4])
        
        return R_index
        
    def Evaluate(self):

        if self.i_rstar < 0:
            self.FindRstar()

        L = SSE.L(self.r[self.i_rstar], self.y[self.i_rstar][0])
        return (self.y[self.i_rstar][3] - L) / np.sqrt(L*self.y[self.i_rstar][3])

    def SurfaceParams(self, i=None):

        if self.i_rstar < 0:
            self.FindRstar()

        return {'T': self.y[self.i_rstar][0], 'rho': self.y[self.i_rstar][1], 'M': self.y[self.i_rstar][2], 'L': self.y[self.i_rstar][3], 'R': self.r[self.i_rstar], 'Tprime': (self.y[self.i_rstar][3] / (4.0 * pi * C.sigma * (self.r[self.i_rstar]**2)))**(1.0/4)}

    def MakePlots(self, prefix=''):
        
        if self.i_rstar < 0:
            self.FindRstar()

        P.StellarInterior(self.y[:self.i_rstar,0], self.y[:self.i_rstar,1], self.y[:self.i_rstar,2], self.y[:self.i_rstar,3], self.epsilon[:self.i_rstar], self.r[:self.i_rstar], prefix)
        P.TransferMeasure(self.dlnPdlnT[:self.i_rstar], self.r[:self.i_rstar], prefix)
        P.StepSize(self.r[:self.i_rstar], prefix)
        P.Opacity(self.kappa[:self.i_rstar], self.r[:self.i_rstar], prefix)
